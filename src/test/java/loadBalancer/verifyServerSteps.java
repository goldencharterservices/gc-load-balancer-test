package loadBalancer;


import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import static io.restassured.RestAssured.given;


public class verifyServerSteps {

    String url = "https://www2.goldencharter.co.uk/";

    @Step("Verify that new website has been loaded")
    public void getNewServer() {
        Response response = given()
                .baseUri(url)
                .header("Cookie", "SRVNAME=NEW")
                .get();
        System.out.println(response.getStatusCode());
        Assert.assertEquals("server=W.O.P.R.", response.getHeaders().get("server").toString());
        Assert.assertEquals("x-server=backendnodes", response.getHeaders().get("x-server").toString());
        String releaseName = response.getHeaders().get("x-gcs-image-ref").toString();
        Assert.assertTrue(releaseName.contains("release_"));

    }

    @Step("Verify that old website has been loaded")
    public void getOldServer() {

        Response response = given()
                .baseUri(url)
                .header("Cookie", "SRVNAME=OLD")
                .get();
        System.out.println(response.getStatusCode());
        Assert.assertEquals("x-server=backendnodes", response.getHeaders().get("x-server").toString());
        Assert.assertFalse(response.getHeaders().hasHeaderWithName("server"));
    }


    @Step("Verify that old website has been loaded")
    public void getOldServerForBots() {
        Response response = given()
                .baseUri(url)
                .header("Cookie", "SRVNAME=NEW").header("User-Agent", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
                .get();
        System.out.println(response.getStatusCode());
        Assert.assertEquals("x-server=seo_crawlers_only", response.getHeaders().get("x-server").toString());


    }


}
