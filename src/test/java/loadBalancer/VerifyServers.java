package loadBalancer;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
public class VerifyServers {
    @Steps
    verifyServerSteps verifyServerSteps;

    @Test
    public void getNewServer() {
        verifyServerSteps.getNewServer();
    }

    @Test
    public void getOldServer() {
        verifyServerSteps.getOldServer();
    }

    @Test
    public void getOldSiteFOrBots() {
        verifyServerSteps.getOldServerForBots();
    }


}
